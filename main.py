###########################################################
#################### USER OPTIONS #########################
###########################################################

# This small program generates a Turing Machine problem that
# can be solved without any additional question to the verifiers
#
# Unless disabled, the generated problem might be a "middle game"
# problem where a few rounds of questions have been done.
#
# Unless disabled, hints are generated together with the problems
# Hints are additional questions that are unnecessary but helpful
#
# The problem generated follows the rules of the game:
# + The solution is unique
# + Every verifier is required to find that solution
#
# The problem is NOT generated according to the same algorithm
# as the official Turing Machine web application. The generation does
# not come with numbers for Check/Pass cards, and does not
# come with an identifier to enter in the Turing Machine
# web application. It instead uses the most simple random
# generation imaginable, which leads to many uninteresting
# and unbalanced configurations.

number_tries_before_giving_up = 100
enable_hints = True # Disabling hints increases the success rate of the algorithm
max_rounds = 1 # The problem will include up to "max_rounds" rounds of tests (with up to 3 tests per round)
max_rounds_with_hints = 2 # The problem with hints will include up to "max_rounds_with_hints" rounds of tests
bottom_up_display = False # Reverse the display order of the problem and solutions

# Some very basic filters are put in place to reject
# problems that seem too easy or too hard to solve
allow_extremely_hard = False
allow_extremely_easy = False

###########################################################
####################### CODE ##############################
###########################################################
debug = False

import copy
import random


value_list = []
for b in [1,2,3,4,5]:
    for j in [1,2,3,4,5]:
        for v in [1,2,3,4,5]:
            value_list = value_list+[(b,j,v)]

def triple(n):
    b = int(n/100)
    assert((b >= 1) and (b <= 5))
    j = int((n % 100)/10)
    assert((j >= 1) and (j <= 5))
    v = n % 10
    assert((v >= 1) and (v <= 5))
    return (b,j,v)

def number(c):
    assert(len(c) == 3)
    return c[0]*100+c[1]*10+c[2]

def number_list(s):
    nl = [number(c) for c in s]
    nl.sort()
    return nl

def next_element(nl):
    if nl is []:
        return []
    else:
        return nl[:-1]+[nl[-1]+1]

def char_to_number(s):
    return (ord(s)-1) % 32

class Verifier():
    def __init__(self,f,sv,text):
        self.constraint = f
        self.values = set(sv)
        self.text = text
    @staticmethod
    def from_constraint(f,text):
        ver = Verifier(f, set(), text)
        ver.values = set(filter(f, value_list))
        return ver
    def __str__(self):
        return str(number_list(self.values))
    def __mul__(self, other):
        return Verifier(self.constraint, self.values.intersection(other.values), self.text)
    def duplicate(self):
        return Verifier(self.constraint, self.values, self.text)
    def apply_filter(self, sv):
        self.values = self.values.intersection(sv)

class Card():
    def __init__(self,name,lv):
        self.name = name
        self.verif_list = lv
    def allowed_codes_list(self):
        return list(filter(lambda v: len(v.values) != 0, self.verif_list))
    def apply_test(self, n, r):
        c = triple(n)
        for v in self.verif_list:
            if ((not r) and (c in v.values)) or (r and (c not in v.values)):
                v.values = set()
    def uncertainty(self):
        return len(self.allowed_codes_list())
    def solutions(self):
        sols = set()
        r = sols.union(*[v.values for v in self.verif_list])
        #print(self.name,len(r))
        return r
    def __str__(self):
        s = ""
        for i in range(len(self.verif_list)):
            s = s +"Ver. " + self.name + "_" + str(i) + " : " + str(self.verif_list[i]) + "\n"
        return s
    def __mul__(self, other):
        return Card(copy.deepcopy(self.name), [v * other for v in self.verif_list])
    def duplicate(self):
        return Card(copy.deepcopy(self.name), [v.duplicate() for v in self.verif_list])
    def apply_filter(self,sv):
        for v in self.verif_list:
            v.apply_filter(sv)

class Problem():
    def __init__(self,cl,tl,sl,name,file):
        self.cards_list = cl
        self.tests_list = tl
        self.solutions_list = sl
        self.name = name
        self.log_file = file
    def init_log_file(self):
        if self.log_file is not None:
            self.log_file.close()
        self.log_file = open("fichier_log.txt", "w")
    def close_log_file(self):
        if self.log_file is not None:
            self.log_file.close()
        self.log_file = None
    @staticmethod
    def without_solutions(cl):
        return Problem(cl,[],[],"",None)
    @classmethod
    def with_solutions(self,cl):
        pb = self.without_solutions(cl)
        pb.compute_solutions()
        return pb
    @staticmethod
    def unique_intersection(l):
        if l is []:
            return None
        else:
            inter = l[0].intersection(*(l[1:]))
            if len(inter) == 1:
                return inter.pop()
            else:
                #print("not unique:",inter)
                return None
    @staticmethod
    def exclude_one(l):
        ll = [[] for elem in range(len(l))]
        for i in range(len(l)):
            for j in range(len(l)):
                if i is not j:
                    ll[i] += [l[j]]
        return ll
    @classmethod
    def check_redundancy(self,l):
        ll = self.exclude_one(l)
        return all([(self.unique_intersection(e) is None) for e in ll])
    @classmethod
    def check_solution(self,l):
        sol = self.unique_intersection(l)
        if sol is None:
            return None
        if self.check_redundancy(l):
            return sol
        else:
            #print(sol,"has redondancies")
            return None
    def extract_configuration(self,nl):
        vll = []
        for i in range(len(self.cards_list)):
            vll += [self.cards_list[i].verif_list[nl[i]].values]
        return vll, next_element(nl)
    def extract_configuration_repeat_until_success(self,nl):
        if len(nl) > len(self.cards_list):
            raise ValueError
        else:
            try:
                nl_full = nl+([0]*(len(self.cards_list)-len(nl)))
                return self.extract_configuration(nl_full)
            except IndexError:
                if len(nl) <= 1:
                    return None,None
                else:
                    return self.extract_configuration_repeat_until_success(next_element(nl[:-1]))
    def compute_solutions(self):
        global debug
        self.solutions_list = []
        nl = [0]
        while nl is not None:
            vll,nl = self.extract_configuration_repeat_until_success(nl)
            if nl is not None:
                sol = self.check_solution(vll)
                if sol is not None:
                    self.solutions_list += [sol]
        if debug:
            print(self.name,"Solutions :", set([number(c) for c in self.solutions_list]))
    def get_naive_solutions(self):
        sols = set(value_list)
        return sols.intersection(*[c.solutions() for c in self.cards_list])

    def apply_test(self,test):
        self.tests_list += [test]
        name_card,n,r = test
        num_card = char_to_number(name_card)
        self.cards_list[num_card].apply_test(n, r)
        #self.compute_solutions()
    def uncertainty(self):
        return sum([c.uncertainty() for c in self.cards_list])
    def __str__(self):
        s = ""
        for c in self.cards_list:
            s += str(c)
        return s
    def __mul__(self, other):
        return Problem([c*other for c in self.cards_list],copy.deepcopy(self.tests_list),copy.deepcopy(self.solutions_list),"",self.log_file)
    def duplicate(self):
        return Problem([c.duplicate() for c in self.cards_list], copy.deepcopy(self.tests_list), copy.deepcopy(self.solutions_list), self.name, self.log_file)
    def apply_filter(self, sv):
        for c in self.cards_list:
            c.apply_filter(sv)
    def log_deep(self,depth,text):
        if self.log_file is not None:
            self.log_file.write(str(depth+1)+": "+("    "*(depth))+text+"\n")
    def check_redundancy_deep(self,depth,ic,iv,index_ver_non_det):
        if ic in index_ver_non_det:
            raise ValueError
        v = self.cards_list[ic].verif_list[iv]
        for icc in index_ver_non_det:
            for ivv in range(len(self.cards_list[icc].verif_list)):
                vv = self.cards_list[icc].verif_list[ivv]
                if len(vv.values) != 0:
                    if all([n in vv.values for n in v.values]):
                        self.cards_list[icc].verif_list[ivv].values = set()
            if self.cards_list[icc].uncertainty() == 0:
                self.log_deep(depth, "=> Hypothesis " + v.text + " make " + self.cards_list[icc].name + " redundant")
                return True
        return False
    def get_solution_deep(self,depth,comp_max,index_ver_non_det):
        result = set()
        #self.log_deep(depth,str(index_ver_non_det))
        if len(index_ver_non_det) == 1:
            ic = index_ver_non_det.copy().pop()
            for iv in range(len(self.cards_list[ic].verif_list)):
                v = self.cards_list[ic].verif_list[iv]
                if len(v.values) == 0:
                    continue
                self.log_deep(depth,"Hypothesis : "+v.text)
                if len(v.values) == 1:
                    self.log_deep(depth,"=> Solution " + str(number(v.values.copy().pop())))
                    result = result.union(v.values)
                if len(v.values) > 1:
                    self.log_deep(depth,"=> Hypothesis " + v.text + " breaks uniqueness")
                    self.cards_list[ic].verif_list[iv].values = set()
            #fichier_log.write("sols "+str(sols)+"\n")
            return result
        if (comp_max <= 0) or (len(index_ver_non_det) == 0):
            return self.get_naive_solutions()
        else:
            restart = True
            while restart:
                #print("    "*(6-prof),"< before",self.uncertainty())
                restart = False
                result = set()
                for ic in index_ver_non_det:
                    others = index_ver_non_det.copy()
                    others.remove(ic)
                    for iv in range(len(self.cards_list[ic].verif_list)):
                        v = self.cards_list[ic].verif_list[iv]
                        new_comp_max = comp_max-1
                        if len(v.values) != 0:
                            if self.cards_list[ic].uncertainty() == 1:
                                new_comp_max = comp_max
                                self.log_deep(depth,"-> Hypothesis required : "+v.text)
                            else:
                                self.log_deep(depth,"Hypothesis : "+v.text)
                            pb = self*v
                            if pb.check_redundancy_deep(depth,ic,iv,others):
                                self.cards_list[ic].verif_list[iv].values = set()
                                restart = True
                                continue
                            sols = pb.get_solution_deep(depth+1,new_comp_max,others)
                            if len(sols) == 0:
                                self.log_deep(depth,"=> Hyptothesis "+v.text+" is contradictory")
                                self.cards_list[ic].verif_list[iv].values = set()
                                restart = True
                                continue
                            #self.log_deep(depth,"=> Giving up on that hypothesis")
                            result = result.union(sols)
                #print("    "*(6-prof),"> after",self.uncertainty())
                if restart:
                    self.log_deep(depth,"------Restart------")
            return result
    def get_solutions_and_min_depth(self):
        self.compute_solutions()
        sols = set(self.solutions_list)
        if len(sols) == 1:
            n = len(self.cards_list)# if borne_comp is None else borne_comp+1
            for comp_max in range(n):
                sols = self.get_solution_deep(0,comp_max,set(range(n)))
                if len(sols) == 1:
                    return comp_max, sols
        return None, sols



ver = [[] for i in range(49)]

n = 1
ver[n].append(Verifier.from_constraint(lambda c: c[0] == 1, "Blue = 1"))
ver[n].append(Verifier.from_constraint(lambda c: c[0] > 1, "Blue > 1"))
n = 2
ver[n].append(Verifier.from_constraint(lambda c: c[0] < 3, "Blue < 3"))
ver[n].append(Verifier.from_constraint(lambda c: c[0] == 3, "Blue = 3"))
ver[n].append(Verifier.from_constraint(lambda c: c[0] > 3, "Blue > 3"))
n = 3
ver[n].append(Verifier.from_constraint(lambda c: c[1] < 3, "Yellow < 3"))
ver[n].append(Verifier.from_constraint(lambda c: c[1] == 3, "Yellow = 3"))
ver[n].append(Verifier.from_constraint(lambda c: c[1] > 3, "Yellow > 3"))
n = 4
ver[n].append(Verifier.from_constraint(lambda c: c[1] < 4, "Yellow < 4"))
ver[n].append(Verifier.from_constraint(lambda c: c[1] == 4, "Yellow = 4"))
ver[n].append(Verifier.from_constraint(lambda c: c[1] > 4, "Yellow > 4"))

n = 5
ver[n].append(Verifier.from_constraint(lambda c: c[0] % 2 == 0, "Blue even"))
ver[n].append(Verifier.from_constraint(lambda c: c[0] % 2 == 1, "Blue odd"))
n = 6
ver[n].append(Verifier.from_constraint(lambda c: c[1] % 2 == 0, "Yellow even"))
ver[n].append(Verifier.from_constraint(lambda c: c[1] % 2 == 1, "Yellow odd"))
n = 7
ver[n].append(Verifier.from_constraint(lambda c: c[2] % 2 == 0, "Purple even"))
ver[n].append(Verifier.from_constraint(lambda c: c[2] % 2 == 1, "Purple odd"))

n = 8
ver[n].append(Verifier.from_constraint(lambda c: [n == 1 for n in c].count(True) == 0, "Zero 1"))
ver[n].append(Verifier.from_constraint(lambda c: [n == 1 for n in c].count(True) == 1, "One 1"))
ver[n].append(Verifier.from_constraint(lambda c: [n == 1 for n in c].count(True) == 2, "Two 1"))
ver[n].append(Verifier.from_constraint(lambda c: [n == 1 for n in c].count(True) == 3, "Three 1"))
n = 9
ver[n].append(Verifier.from_constraint(lambda c: [n == 3 for n in c].count(True) == 0, "Zero 3"))
ver[n].append(Verifier.from_constraint(lambda c: [n == 3 for n in c].count(True) == 1, "One 3"))
ver[n].append(Verifier.from_constraint(lambda c: [n == 3 for n in c].count(True) == 2, "Two 3"))
ver[n].append(Verifier.from_constraint(lambda c: [n == 3 for n in c].count(True) == 3, "Three 3"))
n = 10
ver[n].append(Verifier.from_constraint(lambda c: [n == 4 for n in c].count(True) == 0, "Zero 4"))
ver[n].append(Verifier.from_constraint(lambda c: [n == 4 for n in c].count(True) == 1, "One 4"))
ver[n].append(Verifier.from_constraint(lambda c: [n == 4 for n in c].count(True) == 2, "Two 4"))
ver[n].append(Verifier.from_constraint(lambda c: [n == 4 for n in c].count(True) == 3, "Three 4"))

n = 11
ver[n].append(Verifier.from_constraint(lambda c: c[0] < c[1], "Blue < Yellow"))
ver[n].append(Verifier.from_constraint(lambda c: c[0] == c[1], "Blue = Yellow"))
ver[n].append(Verifier.from_constraint(lambda c: c[0] > c[1], "Blue > Yellow"))
n = 12
ver[n].append(Verifier.from_constraint(lambda c: c[0] < c[2], "Blue < Purple"))
ver[n].append(Verifier.from_constraint(lambda c: c[0] == c[2], "Blue = Purple"))
ver[n].append(Verifier.from_constraint(lambda c: c[0] > c[2], "Blue > Purple"))
n = 13
ver[n].append(Verifier.from_constraint(lambda c: c[1] < c[2], "Yellow < Purple"))
ver[n].append(Verifier.from_constraint(lambda c: c[1] == c[2], "Yellow = Purple"))
ver[n].append(Verifier.from_constraint(lambda c: c[1] > c[2], "Yellow > Purple"))

n = 14
ver[n].append(Verifier.from_constraint(lambda c: (c[0] < c[1]) and (c[0] < c[2]), "Blue < Others"))
ver[n].append(Verifier.from_constraint(lambda c: (c[1] < c[0]) and (c[1] < c[2]), "Yellow < Others"))
ver[n].append(Verifier.from_constraint(lambda c: (c[2] < c[0]) and (c[2] < c[1]), "Purple < Others"))
n = 15
ver[n].append(Verifier.from_constraint(lambda c: (c[0] > c[1]) and (c[0] > c[2]), "Blue > Others"))
ver[n].append(Verifier.from_constraint(lambda c: (c[1] > c[0]) and (c[1] > c[2]), "Yellow > Others"))
ver[n].append(Verifier.from_constraint(lambda c: (c[2] > c[0]) and (c[2] > c[1]), "Purple > Others"))


n = 16
ver[n].append(Verifier.from_constraint(lambda c: [n % 2 == 0 for n in c].count(True) >= 2, "Evens > Odds"))
ver[n].append(Verifier.from_constraint(lambda c: [n % 2 == 0 for n in c].count(True) <= 1, "Odds > Evens"))
n = 17
ver[n].append(Verifier.from_constraint(lambda c: [n % 2 == 0 for n in c].count(True) == 0, "Zero evens"))
ver[n].append(Verifier.from_constraint(lambda c: [n % 2 == 0 for n in c].count(True) == 1, "One even"))
ver[n].append(Verifier.from_constraint(lambda c: [n % 2 == 0 for n in c].count(True) == 2, "Two evens"))
ver[n].append(Verifier.from_constraint(lambda c: [n % 2 == 0 for n in c].count(True) == 3, "Three evens"))

n = 18
ver[n].append(Verifier.from_constraint(lambda c: (c[0] + c[1] + c[2]) % 2 == 0, "Sum even"))
ver[n].append(Verifier.from_constraint(lambda c: (c[0] + c[1] + c[2]) % 2 == 1, "Sum odd"))
n = 19
ver[n].append(Verifier.from_constraint(lambda c: c[0] + c[1] < 6, "Blue + Yellow < 6"))
ver[n].append(Verifier.from_constraint(lambda c: c[0] + c[1] == 6, "Blue + Yellow = 6"))
ver[n].append(Verifier.from_constraint(lambda c: c[0] + c[1] > 6, "Blue + Yellow > 6"))

n = 20
ver[n].append(Verifier.from_constraint(lambda c: (c[0] == c[1]) and (c[1] == c[2]), "Triplets"))
ver[n].append(Verifier.from_constraint(lambda c: ((c[0] == c[1]) and (c[1] != c[2])) or ((c[0] == c[2]) and (c[1] != c[2])) or ((c[1] == c[2]) and (c[0] != c[2])), "Twins"))
ver[n].append(Verifier.from_constraint(lambda c: (c[0] != c[1]) and (c[1] != c[2]) and (c[0] != c[2]), "No twin, no triplets"))
n = 21
ver[n].append(Verifier.from_constraint(lambda c: ((c[0] == c[1]) and (c[1] == c[2])) or ((c[0] != c[1]) and (c[1] != c[2]) and (c[0] != c[2])), "No twins"))
ver[n].append(Verifier.from_constraint(lambda c: ((c[0] == c[1]) and (c[1] != c[2])) or ((c[0] == c[2]) and (c[1] != c[2])) or ((c[1] == c[2]) and (c[0] != c[2])), "Twins"))

n = 22
ver[n].append(Verifier.from_constraint(lambda c: (c[0] < c[1]) and (c[1] < c[2]), "Increasing"))
ver[n].append(Verifier.from_constraint(lambda c: (c[0] > c[1]) and (c[1] > c[2]), "Decreasing"))
ver[n].append(Verifier.from_constraint(lambda c: (not ((c[0] < c[1]) and (c[1] < c[2]))) and (not ((c[0] > c[1]) and (c[1] > c[2]))), "Not increasing, not decreasing"))

n = 23
ver[n].append(Verifier.from_constraint(lambda c: c[0] + c[1] + c[2] < 6, "Sum < 6"))
ver[n].append(Verifier.from_constraint(lambda c: c[0] + c[1] + c[2] == 6, "Sum = 6"))
ver[n].append(Verifier.from_constraint(lambda c: c[0] + c[1] + c[2] > 6, "Sum > 6"))

n = 24
ver[n].append(Verifier.from_constraint(lambda c: (c[0] + 1 == c[1]) and (c[1] + 1 == c[2]), "Three increasing consecutive digits"))
ver[n].append(Verifier.from_constraint(lambda c: ((c[0] + 1 == c[1]) and (c[1] + 1 != c[2])) or ((c[1] + 1 == c[2]) and (c[0] + 1 != c[1])), "Two increasing consecutive digits"))
ver[n].append(Verifier.from_constraint(lambda c: (c[0] + 1 != c[1]) and (c[1] + 1 != c[2]), "No increasing consecutive digits"))
n = 25
ver[n].append(Verifier.from_constraint(lambda c: (c[0] + 1 != c[1]) and (c[0] - 1 != c[1]) and (c[1] + 1 != c[2]) and (c[1] - 1 != c[2]), "Three de/in-creasing consecutive digits"))
ver[n].append(Verifier.from_constraint(lambda c: ((c[0] + 1 == c[1]) and (c[1] + 1 != c[2])) or ((c[0] - 1 == c[1]) and (c[1] - 1 != c[2])) or ((c[1] + 1 == c[2]) and (c[0] + 1 != c[1])) or ((c[1] - 1 == c[2]) and (c[0] - 1 != c[1])), "Two de/in-creasing consecutive digits"))
ver[n].append(Verifier.from_constraint(lambda c: ((c[0] + 1 == c[1]) and (c[1] + 1 == c[2])) or ((c[0] - 1 == c[1]) and (c[1] - 1 == c[2])), "No de/in-creasing consecutive digits"))


n = 26
ver[n].append(Verifier.from_constraint(lambda c: c[0] < 3, "Blue < 3"))
ver[n].append(Verifier.from_constraint(lambda c: c[1] < 3, "Yellow < 3"))
ver[n].append(Verifier.from_constraint(lambda c: c[2] < 3, "Purple < 3"))
n = 27
ver[n].append(Verifier.from_constraint(lambda c: c[0] < 4, "Blue > 4"))
ver[n].append(Verifier.from_constraint(lambda c: c[1] < 4, "Yellow > 4"))
ver[n].append(Verifier.from_constraint(lambda c: c[2] < 4, "Purple > 4"))
n = 28
ver[n].append(Verifier.from_constraint(lambda c: c[0] == 1, "Blue = 1"))
ver[n].append(Verifier.from_constraint(lambda c: c[1] == 1, "Yellow = 1"))
ver[n].append(Verifier.from_constraint(lambda c: c[2] == 1, "Purple = 1"))
n = 29
ver[n].append(Verifier.from_constraint(lambda c: c[0] == 3, "Blue = 3"))
ver[n].append(Verifier.from_constraint(lambda c: c[1] == 3, "Yellow = 3"))
ver[n].append(Verifier.from_constraint(lambda c: c[2] == 3, "Purple = 3"))
n = 30
ver[n].append(Verifier.from_constraint(lambda c: c[0] == 4, "Blue = 4"))
ver[n].append(Verifier.from_constraint(lambda c: c[1] == 4, "Yellow = 4"))
ver[n].append(Verifier.from_constraint(lambda c: c[2] == 4, "Purple = 4"))
n = 31
ver[n].append(Verifier.from_constraint(lambda c: c[0] > 1, "Blue > 1"))
ver[n].append(Verifier.from_constraint(lambda c: c[1] > 1, "Yellow > 1"))
ver[n].append(Verifier.from_constraint(lambda c: c[2] > 1, "Purple > 1"))
n = 32
ver[n].append(Verifier.from_constraint(lambda c: c[0] > 3, "Blue > 3"))
ver[n].append(Verifier.from_constraint(lambda c: c[1] > 3, "Yellow > 3"))
ver[n].append(Verifier.from_constraint(lambda c: c[2] > 3, "Purple > 3"))

n = 33
ver[n].append(Verifier.from_constraint(lambda c: c[0] % 2 == 0, "Blue even"))
ver[n].append(Verifier.from_constraint(lambda c: c[0] % 2 == 1, "Blue odd"))
ver[n].append(Verifier.from_constraint(lambda c: c[1] % 2 == 0, "Yellow even"))
ver[n].append(Verifier.from_constraint(lambda c: c[1] % 2 == 1, "Yellow odd"))
ver[n].append(Verifier.from_constraint(lambda c: c[2] % 2 == 0, "Purple even"))
ver[n].append(Verifier.from_constraint(lambda c: c[2] % 2 == 1, "Purple odd"))

n = 34
ver[n].append(Verifier.from_constraint(lambda c: (c[0] <= c[1]) and (c[0] <= c[2]), "Blue <= Others"))
ver[n].append(Verifier.from_constraint(lambda c: (c[1] <= c[0]) and (c[1] <= c[2]), "Yellow <= Others"))
ver[n].append(Verifier.from_constraint(lambda c: (c[2] <= c[0]) and (c[2] <= c[1]), "Purple <= Others"))
n = 35
ver[n].append(Verifier.from_constraint(lambda c: (c[0] >= c[1]) and (c[0] >= c[2]), "Blue >= Others"))
ver[n].append(Verifier.from_constraint(lambda c: (c[1] >= c[0]) and (c[1] >= c[2]), "Yellow >= Others"))
ver[n].append(Verifier.from_constraint(lambda c: (c[2] >= c[0]) and (c[2] >= c[1]), "Purple >= Others"))

n = 36
ver[n].append(Verifier.from_constraint(lambda c: (c[0] + c[1] + c[2]) % 3 == 0, "Sum divisible by 3"))
ver[n].append(Verifier.from_constraint(lambda c: (c[0] + c[1] + c[2]) % 4 == 0, "Sum divisible by 4"))
ver[n].append(Verifier.from_constraint(lambda c: (c[0] + c[1] + c[2]) % 5 == 0, "Sum divisible by 5"))

n = 37
ver[n].append(Verifier.from_constraint(lambda c: c[0] + c[1] == 4, "Blue + Yellow = 4"))
ver[n].append(Verifier.from_constraint(lambda c: c[0] + c[2] == 4, "Blue + Purple = 4"))
ver[n].append(Verifier.from_constraint(lambda c: c[2] + c[1] == 4, "Yellow + Purple = 4"))
n = 38
ver[n].append(Verifier.from_constraint(lambda c: c[0] + c[1] == 6, "Blue + Yellow = 6"))
ver[n].append(Verifier.from_constraint(lambda c: c[0] + c[2] == 6, "Blue + Purple = 6"))
ver[n].append(Verifier.from_constraint(lambda c: c[2] + c[1] == 6, "Yellow + Purple = 6"))


n = 39
ver[n].append(Verifier.from_constraint(lambda c: c[0] == 1, "Blue = 1"))
ver[n].append(Verifier.from_constraint(lambda c: c[0] > 1, "Blue > 1"))
ver[n].append(Verifier.from_constraint(lambda c: c[1] == 1, "Yellow = 1"))
ver[n].append(Verifier.from_constraint(lambda c: c[1] > 1, "Yellow > 1"))
ver[n].append(Verifier.from_constraint(lambda c: c[2] == 1, "Purple = 1"))
ver[n].append(Verifier.from_constraint(lambda c: c[2] > 1, "Purple > 1"))
n = 40
ver[n].append(Verifier.from_constraint(lambda c: c[0] < 3, "Blue < 3"))
ver[n].append(Verifier.from_constraint(lambda c: c[0] == 3, "Blue = 3"))
ver[n].append(Verifier.from_constraint(lambda c: c[0] > 3, "Blue > 3"))
ver[n].append(Verifier.from_constraint(lambda c: c[1] < 3, "Yellow < 3"))
ver[n].append(Verifier.from_constraint(lambda c: c[1] == 3, "Yellow = 3"))
ver[n].append(Verifier.from_constraint(lambda c: c[1] > 3, "Yellow > 3"))
ver[n].append(Verifier.from_constraint(lambda c: c[2] < 3, "Purple < 3"))
ver[n].append(Verifier.from_constraint(lambda c: c[2] == 3, "Purple = 3"))
ver[n].append(Verifier.from_constraint(lambda c: c[2] > 3, "Purple > 3"))
n = 41
ver[n].append(Verifier.from_constraint(lambda c: c[0] < 4, "Blue < 4"))
ver[n].append(Verifier.from_constraint(lambda c: c[0] == 4, "Blue = 4"))
ver[n].append(Verifier.from_constraint(lambda c: c[0] > 4, "Blue > 4"))
ver[n].append(Verifier.from_constraint(lambda c: c[1] < 4, "Yellow < 4"))
ver[n].append(Verifier.from_constraint(lambda c: c[1] == 4, "Yellow = 4"))
ver[n].append(Verifier.from_constraint(lambda c: c[1] > 4, "Yellow > 4"))
ver[n].append(Verifier.from_constraint(lambda c: c[2] < 4, "Purple < 4"))
ver[n].append(Verifier.from_constraint(lambda c: c[2] == 4, "Purple = 4"))
ver[n].append(Verifier.from_constraint(lambda c: c[2] > 4, "Purple > 4"))

n = 42
ver[n].append(Verifier.from_constraint(lambda c: (c[0] < c[1]) and (c[0] < c[2]), "Blue < Others"))
ver[n].append(Verifier.from_constraint(lambda c: (c[0] > c[1]) and (c[0] > c[2]), "Blue > Others"))
ver[n].append(Verifier.from_constraint(lambda c: (c[1] < c[0]) and (c[1] < c[2]), "Yellow < Others"))
ver[n].append(Verifier.from_constraint(lambda c: (c[1] > c[0]) and (c[1] > c[2]), "Yellow > Others"))
ver[n].append(Verifier.from_constraint(lambda c: (c[2] < c[0]) and (c[2] < c[1]), "Purple < Others"))
ver[n].append(Verifier.from_constraint(lambda c: (c[2] > c[0]) and (c[2] > c[1]), "Purple > Others"))

n = 43
ver[n].append(Verifier.from_constraint(lambda c: c[0] < c[1], "Blue < Yellow"))
ver[n].append(Verifier.from_constraint(lambda c: c[0] == c[1], "Blue = Yellow"))
ver[n].append(Verifier.from_constraint(lambda c: c[0] > c[1], "Blue > Yellow"))
ver[n].append(Verifier.from_constraint(lambda c: c[0] < c[2], "Blue < Purple"))
ver[n].append(Verifier.from_constraint(lambda c: c[0] == c[2], "Blue = Purple"))
ver[n].append(Verifier.from_constraint(lambda c: c[0] > c[2], "Blue > Purple"))
n = 44
ver[n].append(Verifier.from_constraint(lambda c: c[0] < c[1], "Blue < Yellow"))
ver[n].append(Verifier.from_constraint(lambda c: c[0] == c[1], "Blue = Yellow"))
ver[n].append(Verifier.from_constraint(lambda c: c[0] > c[1], "Blue > Yellow"))
ver[n].append(Verifier.from_constraint(lambda c: c[1] < c[2], "Yellow < Purple"))
ver[n].append(Verifier.from_constraint(lambda c: c[1] == c[2], "Yellow = Purple"))
ver[n].append(Verifier.from_constraint(lambda c: c[1] > c[2], "Yellow > Purple"))

n = 45
ver[n].append(Verifier.from_constraint(lambda c: [n == 1 for n in c].count(True) == 0, "Zero 1"))
ver[n].append(Verifier.from_constraint(lambda c: [n == 1 for n in c].count(True) == 1, "One 1"))
ver[n].append(Verifier.from_constraint(lambda c: [n == 1 for n in c].count(True) == 2, "Two 1"))
ver[n].append(Verifier.from_constraint(lambda c: [n == 1 for n in c].count(True) == 3, "Three 1"))
ver[n].append(Verifier.from_constraint(lambda c: [n == 3 for n in c].count(True) == 0, "Zero 3"))
ver[n].append(Verifier.from_constraint(lambda c: [n == 3 for n in c].count(True) == 1, "One 3"))
ver[n].append(Verifier.from_constraint(lambda c: [n == 3 for n in c].count(True) == 2, "Two 3"))
ver[n].append(Verifier.from_constraint(lambda c: [n == 3 for n in c].count(True) == 3, "Three 3"))
n = 46
ver[n].append(Verifier.from_constraint(lambda c: [n == 3 for n in c].count(True) == 0, "Zero 3"))
ver[n].append(Verifier.from_constraint(lambda c: [n == 3 for n in c].count(True) == 1, "One 3"))
ver[n].append(Verifier.from_constraint(lambda c: [n == 3 for n in c].count(True) == 2, "Two 3"))
ver[n].append(Verifier.from_constraint(lambda c: [n == 3 for n in c].count(True) == 3, "Three 3"))
ver[n].append(Verifier.from_constraint(lambda c: [n == 4 for n in c].count(True) == 0, "Zero 4"))
ver[n].append(Verifier.from_constraint(lambda c: [n == 4 for n in c].count(True) == 1, "One 4"))
ver[n].append(Verifier.from_constraint(lambda c: [n == 4 for n in c].count(True) == 2, "Two 4"))
ver[n].append(Verifier.from_constraint(lambda c: [n == 4 for n in c].count(True) == 3, "Three 4"))
n = 47
ver[n].append(Verifier.from_constraint(lambda c: [n == 1 for n in c].count(True) == 0, "Zero 1"))
ver[n].append(Verifier.from_constraint(lambda c: [n == 1 for n in c].count(True) == 1, "One 1"))
ver[n].append(Verifier.from_constraint(lambda c: [n == 1 for n in c].count(True) == 2, "Two 1"))
ver[n].append(Verifier.from_constraint(lambda c: [n == 1 for n in c].count(True) == 3, "Three 1"))
ver[n].append(Verifier.from_constraint(lambda c: [n == 4 for n in c].count(True) == 0, "Zero 4"))
ver[n].append(Verifier.from_constraint(lambda c: [n == 4 for n in c].count(True) == 1, "One 4"))
ver[n].append(Verifier.from_constraint(lambda c: [n == 4 for n in c].count(True) == 2, "Two 4"))
ver[n].append(Verifier.from_constraint(lambda c: [n == 4 for n in c].count(True) == 3, "Three 4"))

n = 48
ver[n].append(Verifier.from_constraint(lambda c: c[0] < c[1], "Blue < Yellow"))
ver[n].append(Verifier.from_constraint(lambda c: c[0] == c[1], "Blue = Yellow"))
ver[n].append(Verifier.from_constraint(lambda c: c[0] > c[1], "Blue > Yellow"))
ver[n].append(Verifier.from_constraint(lambda c: c[0] < c[2], "Blue < Purple"))
ver[n].append(Verifier.from_constraint(lambda c: c[0] == c[2], "Blue = Purple"))
ver[n].append(Verifier.from_constraint(lambda c: c[0] > c[2], "Blue > Purple"))
ver[n].append(Verifier.from_constraint(lambda c: c[1] < c[2], "Yellow < Purple"))
ver[n].append(Verifier.from_constraint(lambda c: c[1] == c[2], "Yellow = Purple"))
ver[n].append(Verifier.from_constraint(lambda c: c[1] > c[2], "Yellow > Purple"))


####################################
card = [None]*49
for i in range(49):
    card[i] = Card("Card #" + str(i), ver[i])


#################################
num_problem = 0
def create_problem(nl):
    global num_problem
    pb = Problem.without_solutions([card[n] for n in nl])
    num_problem += 1
    pb.name = "Problem #"+str(num_problem)+" "+str(nl)
    pb.compute_solutions()
    return pb


def create_problem_random(nb_cards):
    assert (nb_cards >= 4)
    assert (nb_cards <= 6)
    l = [0]*nb_cards
    for i in range(nb_cards):
        c = None
        while (c is None) or (c in l):
            c = random.randint(1,48)
        l[i] = c
    return create_problem(sorted(l))

def random_code():
    return 100*random.randint(1,5)+10*random.randint(1,5)+random.randint(1,5)

#def random_target(nb_cards):
#    return random.choice(['A','B','C','D','E','F'][:nb_cards])

def random_target_list(nb_cards):
    l = ['A','B','C','D','E','F'][:nb_cards]
    random.shuffle(l)
    return l

def problem_evaluation(pb):
    comp_max, sols = pb.duplicate().get_solutions_and_min_depth()
    if len(sols) == 0:
        return None
    if len(sols) > 1:
        return len(sols)+1000
    if comp_max is None:
        raise RuntimeError
    return comp_max

def apply_test(pb,code,target):
    #target = random_target(len(pb.cards_list))
    pb_true = pb.duplicate()
    pb_true.apply_test((target, code, True))
    if len(pb_true.get_naive_solutions()) == 0:
        return None
    v_true = problem_evaluation(pb_true)
    pb_false = pb.duplicate()
    pb_false.apply_test((target, code, False))
    if len(pb_false.get_naive_solutions()) == 0:
        return None
    v_false = problem_evaluation(pb_false)
    #print("Vrai",v_true,"Faux",v_false)
    if (v_false is None) and (v_true is None):
        return None
    if (v_false is None) or (v_true is not None) and (v_true < v_false):
        pb.apply_test((target, code, True))
        return v_true
    if (v_true is None) or (v_false < v_true):
        pb.apply_test((target, code, False))
        return v_false
    pb.apply_test((target, code, random.choice([True, False])))
    return v_true

def create_problem_solvable():
    global debug
    global allow_extremely_hard
    global allow_extremely_easy
    global enable_hints
    global number_tries_before_giving_up
    global max_rounds
    global max_rounds_with_hints
    number_tries_before_giving_up -= 1
    if number_tries_before_giving_up < 0:
        print("Bad luck, try again")
        return None,None,None,None
    min_complexity = 1 if allow_extremely_easy else 2
    max_complexity = 999 if allow_extremely_hard else 2
    nb_cards = random.randint(4,6)
    pb = create_problem_random(nb_cards)
    v = problem_evaluation(pb)
    pb_diff = None
    v_diff = None
    if v is None:
        if debug:
            print("[0] Failure, no solutions")
        return create_problem_solvable()
    if v > max_complexity:
        if debug:
            print("[0] Problem too difficult : "+str(v))
    if v < min_complexity:
        if debug:
            print("[0] Problem too easy")
        return create_problem_solvable()
    if max_complexity >= v >= min_complexity:
        if debug:
            print("[0] Problem found")
        pb_diff = pb.duplicate()
        v_diff = v
        if v <= 1 or not enable_hints:
            return pb_diff,v_diff,None,None
    for j in range(max_rounds_with_hints):
        if j >= max_rounds and pb_diff is None:
            break
        code = random_code()
        targets_list = random_target_list(len(pb.cards_list))
        tests_remaining = 3
        for target in targets_list:
            if debug:
                print(f"[{j+1}] Test",code,target)
            v = apply_test(pb,code,target)
            if v is None:
                if debug:
                    print(f"[{j+1}] Test rejected")
                continue
            if v > max_complexity:
                if debug:
                    print(f"[{j+1}] Problem still too difficult : {v}")
            if v < min_complexity and pb_diff is None:
                if debug:
                    print(f"[{j+1}] Problem too easy")
                return create_problem_solvable()
            if min_complexity <= v <= max_complexity and pb_diff is None:
                if debug:
                    print(f"[{j+1}] Problem found")
                pb_diff = pb.duplicate()
                v_diff = v
                if not enable_hints:
                    return pb_diff,v_diff,None,None
            if v <= 1:
                if debug:
                    print(f"[{j+1}] Hints found")
                return pb_diff,v_diff,pb.duplicate(),v
            tests_remaining -= 1
            if tests_remaining == 0:
                break
    if pb_diff is None or enable_hints:
        if debug:
            print("Failure, no problem found")
    else:
        return None,None,pb_diff,v_diff
    return create_problem_solvable()

def text_from_complexity(n):
    if n <= 0:
        return "Trivial"
    if n == 1:
        return "Easy"
    if n == 2:
        return "Regular"
    if n >= 3:
        return "Hard"

def display_problem(pb_diff,v_diff,v_easy):
    global enable_hints
    print(pb_diff.name)
    print(f"Complexity: {text_from_complexity(v_diff)} ({v_diff})")
    if enable_hints and v_easy is not None:
        print(f"Complexity with hints: {text_from_complexity(v_easy)} ({v_easy})")
    for c in pb_diff.cards_list:
        print("")
        print(c.name)
        for v in c.verif_list:
            print(v.text)
    print("")
    if len(pb_diff.tests_list) != 0:
        print("Given tests for",pb_diff.name)
        for t in pb_diff.tests_list:
            c,n,r = t
            print("Code",n,"on",c,"gives",r)
    else:
        print("No test given for",pb_diff.name)

def display_hints(pb_easy):
    print("###","Given tests (with hints) for",pb_easy.name)
    if pb_easy is not None:
        if len(pb_easy.tests_list) != 0:
            for t in pb_easy.tests_list:
                c,n,r = t
                print("###","Code",n,"on",c,"gives",r)
            return
    print("No test given for",pb_easy.name)


def display_solution(sols):
    print("######","Solution :", set(number_list(sols)))

def describe_problem_solvable():
    global bottom_up_display
    global enable_hints
    pb_diff,v_diff,pb_easy,v_easy = create_problem_solvable()
    if pb_diff is None:
        if v_diff is None and pb_easy is None and v_easy is None:
            return
        pb_diff = pb_easy
        v_diff = v_easy
    pb_diff.compute_solutions()
    sols = pb_diff.solutions_list
    if bottom_up_display:
        display_solution(sols)
        print("---------------^-----------------")
        print("---------------|-----------------")
        print("----------- SOLUTION ------------")
        print("---------------------------------")
        print("---------------------------------")
        if enable_hints:
            display_hints(pb_easy)
            print("---------------^-----------------")
            print("---------------|-----------------")
            print("------------ HINTS --------------")
            print("---------------------------------")
            print("---------------------------------")
        display_problem(pb_diff,v_diff,v_easy)
        print("---------------^-----------------")
        print("---------------|-----------------")
        print("----------- PROBLEM -------------")
        print("---------------------------------")
        print("---------------------------------")
    else:
        print("---------------------------------")
        print("---------------------------------")
        print("----------- PROBLEM -------------")
        print("-------------- | ----------------")
        print("-------------- v ----------------")
        display_problem(pb_diff,v_diff,v_easy)
        if enable_hints:
            print("---------------------------------")
            print("---------------------------------")
            print("------------ HINTS --------------")
            print("-------------- | ----------------")
            print("-------------- v ----------------")
            display_hints(pb_easy)
        print("---------------------------------")
        print("---------------------------------")
        print("----------- SOLUTION ------------")
        print("-------------- | ----------------")
        print("-------------- v ----------------")
        display_solution(sols)





def test():
    problem = [None]*21
    problem[1] = create_problem([4,9,11,14])
    problem[2] = create_problem([4,9,13,17])
    problem[3] = create_problem([3,7,10,14])
    problem[4] = create_problem([3,8,15,16])
    problem[5] = create_problem([2,6,14,17])
    #
    problem[6] = create_problem([2,7,10,13])
    problem[7] = create_problem([8,12,15,17])
    problem[8] = create_problem([3,5,9,15,16])
    problem[9] = create_problem([1,7,10,12,17])
    problem[10] = create_problem([2,6,8,12,15])
    #
    problem[11] = create_problem([5,10,11,15,17])
    problem[12] = create_problem([4,9,18,20])
    problem[13] = create_problem([11,16,19,21])
    problem[14] = create_problem([2,13,17,20])
    problem[15] = create_problem([5,14,18,19,20])
    #
    problem[16] = create_problem([2,7,12,16,19,22])
    problem[17] = create_problem([21,31,37,39])
    problem[18] = create_problem([23,28,41,48])
    problem[19] = create_problem([19,24,30,31,38])
    problem[20] = create_problem([11,22,30,33,34,40])
    print("----------------------------------")
    #pb = problem[9]
    #print(pb)
    #pb.compute_solutions()
    #print()
    pb = create_problem([1,4,20,40])
    pb.apply_test(("D",345,False))
    pb.apply_test(("B",345,False))
    pb.apply_test(("D",212,False))
    pb.apply_test(("B",212,True))
    #pb = problem[9]
    #pb = create_problem_random(5)
    pb.init_log_file()
    #comp_max = 5
    #sols = pb.get_solution_deep(0,comp_max,set(range(len(pb.cards_list))))
    comp_max,sols = pb.get_solutions_and_min_depth()
    print("Comp =", comp_max,"Nb = ", len(sols),"List =", number_list(sols))
    pb.log_file.write("\n\n"+str(pb))
    #pb.compute_solutions()
    #pb.apply_test(("A",411,True))
    #pb.apply_test(("B",411,True))
    #pb.apply_test(("C",411,True))
    #pb.apply_test(("D",411,True))
    #pb.apply_test(("E",411,True))
    #pb.apply_test(("F",411,True))
    #print(pb)
    #print(liste_nombre(pb.get_naive_solutions()))
    #pb.compute_solutions()
    pb.close_log_file()

###########################################################
##################### EXECUTION ###########################
###########################################################

describe_problem_solvable()

